import * as httplease from 'httplease';

export type ShouldRetryFunction = (info: {
    error: httplease.errors.HttpleaseError;
    attempts: number;
    requestConfig: httplease.Builder['config'];
}) => boolean;

export interface FailoverRetryConfig {
    baseUrlList?: string[];
    maxAttempts: number;
    retryDelayMillis?: number;
    shouldRetryFn: ShouldRetryFunction;
}

export interface BackoffRetryConfig {
    retryTimeoutMillis: number;
    shouldRetryFn?: ShouldRetryFunction;
}

export function createRetryFilter(config: FailoverRetryConfig): any;

export function createFailoverRetryFilter(config: FailoverRetryConfig): any;

export function createBackoffRetryFilter(config: BackoffRetryConfig): any;
