'use strict';

const delay = require('./delay');

function createFailoverRetryFilter({baseUrlList, maxAttempts, retryDelayMillis = 0, shouldRetryFn}) {
    async function retryFilter(requestConfig, next) {
        let baseUrlIndex = 0;
        let attempts = 1;

        for (;;) {
            updateBaseUrl(requestConfig, baseUrlIndex);

            try {
                return await next(requestConfig);
            } catch (error) {
                if (attempts >= maxAttempts || !shouldRetryFn({error, attempts, requestConfig})) {
                    error.totalFailedAttempts = attempts;
                    throw error;
                }

                attempts++;

                await delay(retryDelayMillis);

                baseUrlIndex++;
            }
        }
    }

    function updateBaseUrl(requestConfig, baseUrlIndex) {
        if (baseUrlList) {
            requestConfig.baseUrl = baseUrlList[baseUrlIndex % baseUrlList.length];
            requestConfig.httpOptions.protocol = null;
        }
    }

    if (baseUrlList) {
        retryFilter.order = -10;
    }

    return retryFilter;

}

module.exports = createFailoverRetryFilter;

