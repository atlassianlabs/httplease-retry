'use strict';

const createFailoverRetryFilter = require('./lib/failover-retry-filter');

module.exports = {
    createRetryFilter: createFailoverRetryFilter,
    createFailoverRetryFilter,
    createBackoffRetryFilter: require('./lib/backoff-retry-filter')
};
