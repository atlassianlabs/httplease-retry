'use strict';

const jasmineHttpSpy = require('jasmine-http-server-spy');
const httplease = require('httplease');

const {createBackoffRetryFilter} = require('../..');

describe('integration/backoff-retry-Filter', () => {

    let httpSpy;

    beforeEach(() => {
        httpSpy = jasmineHttpSpy.createSpyObj('mockServer', [
            {
                handlerName: 'getHandler',
                method: 'get',
                url: '/get'
            }
        ]);

        return httpSpy.server.start(8082);
    });

    afterEach(() => {
        return httpSpy.server.stop();
    });

    function newHttpClient(extraRetryConfig) {
        const defaultRetryConfig = {
            retryTimeoutMillis: 60 * 1000
        };

        return httplease.builder()
            .withTimeout(5000)
            .withMethodGet()
            .withExpectStatus([200])
            .withBufferJsonResponseHandler()
            .withFilter(createBackoffRetryFilter(Object.assign({}, defaultRetryConfig, extraRetryConfig)))
            .withBaseUrl('http://localhost:8082/get');
    }

    it('should retry on 503 and 429', () => {
        httpSpy.getHandler.and.returnValues(
            {statusCode: 503},
            {statusCode: 429},
            {statusCode: 200, body: {some: 'body'}}
        );

        return newHttpClient({baseUrlList: null})
            .withBaseUrl('http://localhost:8082/get')
            .send()
            .then((result) => {
                expect(httpSpy.getHandler.calls.count()).toBe(3);
                expect(result.body).toEqual({some: 'body'});
            });
    });

    it('should not retry on successful attempt', () => {
        httpSpy.getHandler.and.returnValues({statusCode: 200, body: {some: 'body'}}, {statusCode: 200});

        return newHttpClient().send()
            .then((result) => {
                expect(httpSpy.getHandler.calls.count()).toBe(1);
                expect(result.body).toEqual({some: 'body'});
            });
    });

    it('should not retry if the retryTimeoutMillis is exceeded', () => {
        httpSpy.getHandler.and.returnValues({statusCode: 429}, {statusCode: 503});

        return expectToReject(newHttpClient({retryTimeoutMillis: 0}).send())
            .then((error) => {
                expect(httpSpy.getHandler.calls.count()).toBe(1);
                expect(error.response.statusCode).toEqual(429);
            });
    });

});
