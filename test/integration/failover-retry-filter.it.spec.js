'use strict';

const jasmineHttpSpy = require('jasmine-http-server-spy');
const httplease = require('httplease');

const {createRetryFilter, createFailoverRetryFilter} = require('../..');

describe('integration/failover-retry-Filter', () => {

    let httpSpy;

    beforeEach(() => {
        httpSpy = jasmineHttpSpy.createSpyObj('mockServer', [
            {
                handlerName: 'getHandler1',
                method: 'get',
                url: '/get1'
            },
            {
                handlerName: 'getHandler2',
                method: 'get',
                url: '/get2'
            }
        ]);

        return httpSpy.server.start(8082);
    });

    afterEach(() => {
        return httpSpy.server.stop();
    });

    function newHttpClient(extraRetryConfig) {
        const defaultRetryConfig = {
            shouldRetryFn: () => true,
            baseUrlList: ['http://localhost:8082/get1', 'http://localhost:8082/get2'],
            maxAttempts: 5
        };

        return httplease.builder()
            .withTimeout(5000)
            .withMethodGet()
            .withExpectStatus([200])
            .withBufferJsonResponseHandler()
            .withFilter(createFailoverRetryFilter(Object.assign({}, defaultRetryConfig, extraRetryConfig)));
    }

    it('should call the original base URL if no baseUrlList is given', () => {
        httpSpy.getHandler1.and.returnValues(
            {statusCode: 500},
            {statusCode: 200, body: {some: 'body'}}
        );

        return newHttpClient({baseUrlList: null})
            .withBaseUrl('http://localhost:8082/get1')
            .send()
            .then((result) => {
                expect(httpSpy.getHandler1.calls.count()).toBe(2);
                expect(httpSpy.getHandler2.calls.count()).toBe(0);
                expect(result.body).toEqual({some: 'body'});
            });
    });

    it('should retry the alternate URL if an unexpected status is given', () => {
        httpSpy.getHandler1.and.returnValue({statusCode: 500});
        httpSpy.getHandler2.and.returnValue({statusCode: 200, body: {some: 'body'}});

        return newHttpClient().send()
            .then((result) => {
                expect(httpSpy.getHandler1.calls.count()).toBe(1);
                expect(httpSpy.getHandler2.calls.count()).toBe(1);
                expect(result.body).toEqual({some: 'body'});
            });
    });

    it('should not call the alternate URL if a successful response occurs', () => {
        httpSpy.getHandler1.and.returnValue({statusCode: 200, body: {some: 'body'}});
        httpSpy.getHandler2.and.returnValue({statusCode: 200});

        return newHttpClient().send()
            .then((result) => {
                expect(httpSpy.getHandler1.calls.count()).toBe(1);
                expect(httpSpy.getHandler2.calls.count()).toBe(0);
                expect(result.body).toEqual({some: 'body'});
            });
    });

    it('should only retry maxAttempts number of times', () => {
        httpSpy.getHandler1.and.returnValue({statusCode: 404});
        httpSpy.getHandler2.and.returnValue({statusCode: 404});

        return expectToReject(newHttpClient().send())
            .then((error) => {
                expect(httpSpy.getHandler1.calls.count()).toBe(3);
                expect(httpSpy.getHandler2.calls.count()).toBe(2);
                expect(error.response.statusCode).toEqual(404);
            });
    });

    it('legacy createRetryFilter interface points to createFailoverRetryFilter', () => {
        expect(createRetryFilter).toBe(createFailoverRetryFilter);
    });

});
