'use strict';

const requireWithMocks = require('proxyquire').noCallThru().noPreserveCache();

describe('createFailoverRetryFilter', () => {

    let shouldRetryFn;
    let fakeNext;
    let fakeNextResults;
    let delayMock;
    let requestConfig;
    let createRetryFilter;

    beforeEach(() => {
        shouldRetryFn = jasmine.createSpy('shouldRetryFn');
        shouldRetryFn.and.returnValue(true);

        delayMock = jasmine.createSpy('delayMock');
        fakeNext = jasmine.createSpy('fakeNext');
        fakeNextResults = [() => Promise.resolve()];
        let fakeNextCounter = 0;
        fakeNext.and.callFake(() => {
            const fn = fakeNextResults[fakeNextCounter];
            fakeNextCounter++;
            return fn();
        });

        createRetryFilter = requireWithMocks('../../lib/failover-retry-filter', {
            './delay': delayMock
        });

        requestConfig = {
            someConfig: true,
            httpOptions: {},
            baseUrl: 'original-base-url'
        };
    });

    function createFilter(extraConfig) {
        const defaultConfig = {
            baseUrlList: ['url-a', 'url-b'],
            maxAttempts: 5,
            retryDelayMillis: 0,
            shouldRetryFn
        };

        return createRetryFilter(Object.assign({}, defaultConfig, extraConfig));
    }

    function runFilter(extraConfig) {
        const filter = createFilter(extraConfig);
        return filter(requestConfig, fakeNext);
    }

    it('calls next with the requestConfig', async () => {
        await runFilter();

        expect(fakeNext).toHaveBeenCalledWith(requestConfig);
    });

    it('returns the result of calling next', async () => {
        fakeNextResults = [() => Promise.resolve('some result')];

        const result = await runFilter();

        expect(result).toBe('some result');
    });

    it('clears httpOptions.protocol so that it will be recalculated by httplease', async () => {
        requestConfig.httpOptions.protocol = 'something';
        fakeNextResults = [
            () => Promise.reject(new Error('fake error')),
            () => Promise.resolve('good result')
        ];

        await runFilter();

        expect(requestConfig.httpOptions.protocol).toBe(null);
    });

    it('sets the order property so this filter runs before populateHttpOptions', () => {
        const filter = createFilter();

        expect(filter.order).toBeLessThan(0);
    });


    it('should only try maxAttempts number of times', async () => {
        fakeNextResults = [
            () => Promise.reject(new Error('fake error 1')),
            () => Promise.reject(new Error('fake error 2')),
            () => Promise.reject(new Error('fake error 3')),
            () => Promise.reject(new Error('fake error 4')),
            () => Promise.reject(new Error('fake error 5')),
            () => Promise.reject(new Error('fake error 6 never seen'))
        ];

        const error = await expectToReject(runFilter());

        expect(fakeNext.calls.count()).toBe(5);
        expect(error.message).toBe('fake error 5');
    });

    it('should only try while shouldRetry returns true', async () => {
        fakeNextResults = [
            () => Promise.reject(new Error('fake error 1')),
            () => Promise.reject(new Error('fake error 2')),
            () => Promise.reject(new Error('fake error 3 never seen'))
        ];

        shouldRetryFn.and.returnValues(true, false);

        const error = await expectToReject(runFilter());

        expect(fakeNext.calls.count()).toBe(2);
        expect(shouldRetryFn.calls.count()).toBe(2);
        expect(error.message).toBe('fake error 2');
    });

    it('adds the number of retry attempts to errors which are not retried', async () => {
        fakeNextResults = [
            () => Promise.reject(new Error('fake error 1')),
            () => Promise.reject(new Error('fake error 2')),
            () => Promise.reject(new Error('fake error 3')),
            () => Promise.reject(new Error('fake error 4'))
        ];

        shouldRetryFn.and.returnValues(true, true, true, false);

        const error = await expectToReject(runFilter());
        expect(error.message).toBe('fake error 4');
        expect(error.totalFailedAttempts).toBe(4);
    });

    it('calls shouldRetryFn with the correct parameters', async () => {
        fakeNextResults = [
            () => Promise.reject(new Error('fake error')),
            () => Promise.resolve('good result')
        ];

        await runFilter();
        expect(shouldRetryFn).toHaveBeenCalledWith({
            error: new Error('fake error'),
            attempts: 1,
            requestConfig
        });
    });

    it('delays with the configured amount', async () => {
        fakeNextResults = [
            () => Promise.reject(new Error('fake error')),
            () => Promise.resolve('good result')
        ];

        const result = await runFilter({retryDelayMillis: 1000});

        expect(fakeNext).toHaveBeenCalledTimes(2);
        expect(result).toBe('good result');
        expect(delayMock).toHaveBeenCalledWith(1000);
    });

    it('default delay is 0', async () => {
        fakeNextResults = [
            () => Promise.reject(new Error('fake error')),
            () => Promise.resolve('good result')
        ];

        const result = await runFilter();

        expect(fakeNext).toHaveBeenCalledTimes(2);
        expect(result).toBe('good result');
        expect(delayMock).toHaveBeenCalledWith(0);
    });

    describe('with baseUrlList unset', () => {
        it('should not touch the baseUrl', async () => {
            requestConfig.baseUrl = 'the-untouched-base-url';
            await runFilter({baseUrlList: undefined});

            expect(fakeNext.calls.count()).toBe(1);
            expect(requestConfig.baseUrl).toEqual('the-untouched-base-url');
        });

        it('should not set the order property', () => {
            const filter = createFilter({baseUrlList: null});

            expect(filter.order).not.toBeDefined();
        });
    });

    describe('with baseUrlList', () => {
        let fakeNextCallsArray;

        beforeEach(() => {
            fakeNextCallsArray = [];
            let fakeNextCounter = 0;

            /* We have to store the calls that have been made to fakeNext as
               toHaveBeenCalledWith will have gotten the same modified variable each time
               and then you can not verify that it was called with specific values */
            fakeNext.and.callFake((args) => {
                fakeNextCallsArray.push(Object.assign({}, args));
                const fn = fakeNextResults[fakeNextCounter];
                fakeNextCounter++;
                return fn();
            });
        });

        it('should use the first URL for the first request', async () => {
            await runFilter({baseUrlList: ['url-a']});

            expect(fakeNext.calls.count()).toBe(1);
            expect(requestConfig.baseUrl).toEqual('url-a');
        });

        it('should use second URL for second attempt', async () => {
            fakeNextResults = [
                () => Promise.reject(new Error('fake error')),
                () => Promise.resolve('good result')
            ];

            const result = await runFilter({baseUrlList: ['url-a', 'url-b']});

            expect(fakeNext.calls.count()).toBe(2);
            expect(fakeNext).toHaveBeenCalledWith(
                jasmine.objectContaining({baseUrl: 'url-b'})
            );
            expect(requestConfig.baseUrl).toEqual('url-b');
            expect(result).toBe('good result');
        });

        it('should cycle through the URL list', async () => {
            fakeNextResults = [
                () => Promise.reject(new Error('fake error')),
                () => Promise.reject(new Error('fake error')),
                () => Promise.reject(new Error('fake error')),
                () => Promise.resolve('good result')
            ];
            await runFilter({baseUrlList: ['url-a', 'url-b', 'url-c']});

            expect(fakeNextCallsArray.map(({baseUrl}) => baseUrl))
                .toEqual(['url-a', 'url-b', 'url-c', 'url-a']);
            expect(requestConfig.baseUrl).toEqual('url-a');
        });
    });
});
