'use strict';

const requireWithMocks = require('proxyquire').noCallThru().noPreserveCache();

describe('backoffRetryFilter', () => {
    let fakeNext;
    let fakeNextResults;
    let delayMock;
    let requestConfig;
    let createRetryFilter;

    beforeEach(() => {

        delayMock = jasmine.createSpy('delayMock');
        delayMock.and.stub();
        fakeNext = jasmine.createSpy('fakeNext');
        fakeNextResults = [() => Promise.resolve()];
        let fakeNextCounter = 0;
        fakeNext.and.callFake(() => {
            const fn = fakeNextResults[fakeNextCounter];
            fakeNextCounter++;
            return fn();
        });

        createRetryFilter = requireWithMocks('../../lib/backoff-retry-filter', {
            './delay': delayMock
        });

        requestConfig = {
            someConfig: true,
            httpOptions: {},
            baseUrl: 'original-base-url'
        };
    });

    function createDefaultError({msg = 'expected fake error', headers = {dummy: ''}, statusCode = 503} = {}) {
        const error = new Error(msg);
        error.response = {headers, statusCode};

        return error;
    }

    function createFilter(extraConfig) {
        const defaultConfig = {retryTimeoutMillis: 60 * 1000};

        return createRetryFilter(Object.assign({}, defaultConfig, extraConfig));
    }

    function runFilter(extraConfig) {
        const filter = createFilter(extraConfig);
        return filter(requestConfig, fakeNext);
    }

    it('calls next with the requestConfig', async () => {
        await runFilter();

        expect(fakeNext).toHaveBeenCalledWith(requestConfig);
    });

    it('returns the result of calling next', async () => {
        fakeNextResults = [() => Promise.resolve('some result')];

        const result = await runFilter();

        expect(result).toBe('some result');
    });

    it('fails to create filter if retry retryTimeoutMillis is not set', () => {
        expect(() => createFilter({retryTimeoutMillis: undefined}))
            .toThrow(jasmine.objectContaining({message: 'retryTimeoutMillis has to be a non-negative number'}));
    });

    it('should not retry if retryTimeoutMillis is 0', async () => {
        const expectedError = createDefaultError();
        fakeNextResults = [
            () => Promise.reject(expectedError),
            () => Promise.resolve('good result')
        ];

        const error = await expectToReject(runFilter({retryTimeoutMillis: 0}));
        expect(error).toBe(expectedError);
    });

    it('should add number of failed attempts to error on no more retries', async () => {
        fakeNext.and.callFake(() => Promise.reject(createDefaultError()));

        const shouldRetryFn = ({attempts}) => attempts < 4;

        const error = await expectToReject(runFilter({shouldRetryFn}));
        expect(error.totalFailedAttempts).toEqual(4);
    });

    describe('customShouldRetryFn', () => {
        it('rejects when custom criteria is not matched', async () => {
            const expectedError = new Error('error');
            const shouldRetryFn = () => false;

            fakeNextResults = [
                () => Promise.reject(expectedError),
                () => Promise.reject(expectedError)
            ];

            const error = await expectToReject(runFilter({shouldRetryFn}));

            expect(error).toBe(expectedError);
            expect(fakeNext).toHaveBeenCalledTimes(1);
        });

        it('retries when custom criteria is matched', async () => {
            const expectedError = new Error('error');
            const shouldRetryFn = () => true;

            fakeNextResults = [
                () => Promise.reject(expectedError),
                () => Promise.reject(expectedError),
                () => Promise.resolve('good result')
            ];

            const finalResult = await runFilter({shouldRetryFn});

            expect(finalResult).toBe('good result');
            expect(fakeNext).toHaveBeenCalledTimes(3);
        });

        it('should be called with correct params', async () => {
            const shouldRetryFn = jasmine.createSpy('shouldRetryFn');
            shouldRetryFn.and.returnValue(true);
            const expectedError = new Error('error');

            fakeNextResults = [
                () => Promise.reject(expectedError),
                () => Promise.resolve('good result')
            ];

            await runFilter({shouldRetryFn});

            expect(shouldRetryFn).toHaveBeenCalledWith({
                error: expectedError,
                attempts: 1,
                requestConfig,
                startTimestamp: jasmine.any(Number)
            });
        });
    });

    describe('defaultShouldRetryFn', () => {
        it('rejects on status codes other than 429 and 503', async () => {
            const expectedError = createDefaultError({statusCode: 400});
            fakeNextResults = [
                () => Promise.reject(expectedError)
            ];

            const error = await expectToReject(runFilter());

            expect(error).toBe(expectedError);
        });

        it('retries on 429 but not on other status coeds', async () => {
            const expectedError = createDefaultError({statusCode: 404});
            fakeNextResults = [
                () => Promise.reject(createDefaultError({statusCode: 429})),
                () => Promise.reject(expectedError)
            ];

            const error = await expectToReject(runFilter());

            expect(error).toBe(expectedError);
        });

        it('retries on 503', async () => {
            const expectedError = createDefaultError({statusCode: 500});
            fakeNextResults = [
                () => Promise.reject(createDefaultError({statusCode: 429})),
                () => Promise.reject(expectedError)
            ];

            const error = await expectToReject(runFilter());

            expect(error).toBe(expectedError);
        });

        it('rejects on missing response', async () => {
            const expectedError = new Error('error without response');
            fakeNextResults = [
                () => Promise.reject(expectedError)
            ];

            const error = await expectToReject(runFilter());

            expect(error).toBe(expectedError);
        });
    });

    describe('mocked clock tests', () => {
        beforeEach(() => {
            jasmine.clock().install();
            jasmine.clock().mockDate();
        });

        afterEach(() => {
            jasmine.clock().uninstall();
        });

        function actAsNextConsumesTime(serverDelayMs) {
            let fakeNextCounter = 0;
            fakeNext.and.callFake(() => {
                const fn = fakeNextResults[fakeNextCounter];
                fakeNextCounter++;
                jasmine.clock().tick(serverDelayMs);
                return fn();
            });
        }

        it('stops retrying when retryTimeoutMillis is reached', async () => {
            const expectedError = createDefaultError({msg: 'this one'});
            fakeNextResults = [
                () => Promise.reject(createDefaultError({msg: 'not this one'})),
                () => Promise.reject(createDefaultError({msg: 'not this one'})),
                () => Promise.reject(expectedError)
            ];

            const eachCallTime = 3000;
            const promise = runFilter({retryTimeoutMillis: eachCallTime * 3});

            actAsNextConsumesTime(eachCallTime);

            for (let i = 0; i < 1; i++) {
                await Promise.resolve();
                jasmine.clock().tick(1);
            }

            const error = await expectToReject(promise);
            expect(error).toBe(expectedError);
        });

        it('should add totalWaitingTime to error on no more retries', async () => {
            const shouldRetryFn = ({attempts}) => attempts < 4;
            const expectedError = createDefaultError({msg: 'this one'});
            fakeNextResults = [
                () => Promise.reject(createDefaultError({msg: 'not this one'})),
                () => Promise.reject(createDefaultError({msg: 'not this one'})),
                () => Promise.reject(expectedError)
            ];
            delayMock.and.callFake((delay) => jasmine.clock().tick(delay));

            const eachCallTime = 10;
            const promise = runFilter({shouldRetryFn});

            actAsNextConsumesTime(eachCallTime);


            const error = await expectToReject(promise);
            expect(error.totalWaitingTime).toEqual(32 + 64 + 30);
            expect(error).toBe(expectedError);
        });
    });

    describe('backs off', () => {

        function expectDelayToHaveBeenCalledInOrder(...args) {
            expect(delayMock.calls.allArgs().map(([arg]) => arg)).toEqual(args);
        }

        it('exponentially when no retry-after header is available (starts on 32)', async () => {
            fakeNextResults = [
                () => Promise.reject(createDefaultError()),
                () => Promise.reject(createDefaultError()),
                () => Promise.reject(createDefaultError()),
                () => Promise.reject(createDefaultError()),
                () => Promise.reject(createDefaultError()),
                () => Promise.resolve('good result')
            ];

            await runFilter();

            expectDelayToHaveBeenCalledInOrder(0, 32, 64, 128, 256);
        });

        it('do not exceed a maximum delay of a quarter of the maximum', async () => {
            fakeNextResults = [
                () => Promise.reject(createDefaultError()),
                () => Promise.reject(createDefaultError()),
                () => Promise.reject(createDefaultError()),
                () => Promise.reject(createDefaultError()),
                () => Promise.reject(createDefaultError()),
                () => Promise.resolve('good result')
            ];

            const result = await runFilter({retryTimeoutMillis: 256});

            expectDelayToHaveBeenCalledInOrder(0, 32, 64, 64, 64);
            expect(result).toEqual('good result');
        });

        it('rounds down maximum delay', async () => {
            fakeNextResults = [
                () => Promise.reject(createDefaultError()),
                () => Promise.reject(createDefaultError()),
                () => Promise.reject(createDefaultError()),
                () => Promise.resolve('good result')
            ];

            const result = await runFilter({retryTimeoutMillis: 254});

            expectDelayToHaveBeenCalledInOrder(0, 32, 63);
            expect(result).toEqual('good result');
        });

        it('waits the amount of time the retry-after header specifies', async () => {
            fakeNextResults = [
                () => Promise.reject(createDefaultError({headers: {'retry-after': 2}})),
                () => Promise.reject(createDefaultError({headers: {'retry-after': 15}})),
                () => Promise.resolve('good result')
            ];

            await runFilter();

            expectDelayToHaveBeenCalledInOrder(2000, 15000);
        });

        it('uses retry-after when available', async () => {
            fakeNextResults = [
                () => Promise.reject(createDefaultError({headers: {'retry-after': 2}})),
                () => Promise.reject(createDefaultError()),
                () => Promise.reject(createDefaultError()),
                () => Promise.reject(createDefaultError({headers: {'retry-after': 5}})),
                () => Promise.reject(createDefaultError()),
                () => Promise.resolve('good result')
            ];

            await runFilter();

            expect(delayMock.calls.allArgs()).toEqual([
                [2000],
                [32],
                [64],
                [5000],
                [256]
            ]);
        });

        it('uses exponential backoff if retry-after isn\'t parsable to int', async () => {
            fakeNextResults = [
                () => Promise.reject(createDefaultError({headers: {'retry-after': 'adfnunaf'}})),
                () => Promise.reject(createDefaultError({headers: {'retry-after': new Date()}})),
                () => Promise.resolve('good result')
            ];

            await runFilter();

            expectDelayToHaveBeenCalledInOrder(0, 32);
        });
    });

});
