# httplease-retry

Provides two different retry mechanisms, backoff and failover retrying as an [httplease](https://bitbucket.org/atlassianlabs/httplease) filter.

Failover retry filter retries requests with optional fallback to alternate servers.
Backoff retry filter will retry against the same server until the retryTimeout limit has been reached. 
By default it  will retry if 429s and 503s are received, but this logic can be changed by specifying 'shouldRetryFn'.


# Usage guide

Install the library:

```
npm install --save httplease-retry
```

For more examples have a look at the `test/integration` directory.


## Example - Failover retry 

```
const httplease = require('httplease');
const httpleaseRetry = require('httplease-retry');


const retryConfig = {
    shouldRetryFn: ({
        error, // The error from httplease
        attempts, // Number of attempts
        requestConfig, // httplease requestConfig
    }) => true,
    maxAttempts: 2,
    retryDelayMillis: 50,
    baseUrlList: [
        'http://example.com/basePath',
        'http://example.com/secondaryPath
   ]
};

// this can be saved and reused as many times as you want
const httpClient = httplease.builder()
    .withFilter(httpleaseRetry.createFailoverRetryFilter(retryConfig));


// make a request
httpClient
    .withPath('/some-resource')
    .withMethodGet()
    .send();
```


## Example - Backoff retry 

```
const httplease = require('httplease');
const httpleaseRetry = require('httplease-retry');

const retryConfig = {
    retryTimeoutMillis: 10 * 1000 //Retries for a maximum of 10 seconds after the Request started
    shouldRetryFn: ({
        error, // The error from httplease
        attempts, // Number of attempts
        requestConfig, // httplease requestConfig
        startTimestamp // The timestamp the when the request started
   }) = attempts > 5 // Example to stop retrying after 5 attempts
};

// this can be saved and reused as many times as you want
const httpClient = httplease.builder()
    .withFilter(httpleaseRetry.createBackoffRetryFilter(retryConfig));


// make a request
httpClient
    .withPath('/some-resource')
    .withMethodGet()
    .send();
```

# Development guide

## Install dependencies

```
npm install
```


## Useful commands

```
# Run all checks
npm test

# Run just the jasmine tests
npm run test:jasmine

# Run just the linter
npm run test:lint
```


## Perform a release

```
npm version 99.98.97
npm publish
git push
git push --tags
```

## Contributors

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement, known as a CLA. This serves as a record stating that the contributor is entitled to contribute the code/documentation/translation to the project and is willing to have it used in distributions and derivative works (or is willing to transfer ownership).

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)


# License

Copyright (c) 2017 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.
